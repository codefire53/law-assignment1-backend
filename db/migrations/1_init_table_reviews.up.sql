
-- +migrate Up

-- SEQUENCE: public.reviews_id_seq
CREATE SEQUENCE reviews_id_seq;

-- Table: public.customers
CREATE TABLE reviews
(
    id integer NOT NULL DEFAULT nextval('reviews_id_seq'::regclass),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    title character varying(300) COLLATE pg_catalog."default" NOT NULL,
    preview character varying(500) COLLATE pg_catalog."default" NOT NULL,
    anime character varying(250) COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default" NOT NULL,
    image text COLLATE pg_catalog."default",
    CONSTRAINT reviews_pkey PRIMARY KEY (id)
);

-- Index: idx_customers_deleted_at
CREATE INDEX idx_reviews_deleted_at
    ON reviews USING btree
    (deleted_at)
    TABLESPACE pg_default;

-- +migrate Down

-- +migrate Up

-- +migrate Down
