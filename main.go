package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	reviewControllers "gitlab.com/codefire53/law-assignment1-backend/controllers/review"
	"gitlab.com/codefire53/law-assignment1-backend/repositories"
	reviewServices "gitlab.com/codefire53/law-assignment1-backend/services/review"
)

func main() {
	router := mux.NewRouter()
	reviewRepository := repositories.ReviewRepositoryImpl{}
	reviewService := reviewServices.ReviewServiceImpl{
		ReviewRepository: reviewRepository,
	}
	reviewController := reviewControllers.ReviewController{
		ReviewService: reviewService,
	}
	router.HandleFunc("/api/get-reviews", reviewController.GetReviews).Methods("GET")
	router.HandleFunc("/api/get-review/{id}", reviewController.GetReviewDetail).Methods("GET")
	router.HandleFunc("/api/update-review/{id}", reviewController.UpdateReview).Methods("PUT")
	router.HandleFunc("/api/delete-review/{id}", reviewController.DeleteReview).Methods("DELETE")
	router.HandleFunc("/api/create-review", reviewController.CreateReview).Methods("POST")
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "PUT", "POST", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Origin", "Content-Type", "X-Auth-Token", "Accept", "Authorization"},
	})
	server := c.Handler(router)
	fmt.Println("Listening at port :8080")

	//err := http.ListenAndServe(":8080", router)
	err := http.ListenAndServe(":"+os.Getenv("PORT"), server) //Launch the app, visit localhost:8000/api
	if err != nil {
		fmt.Print(err)
	}

}
