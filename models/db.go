package models

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB

func init() {
	//Local development or non heroku host
	/*
		username := os.Getenv("db_username")
		password := os.Getenv("db_password")
		dbName := os.Getenv("db_name")
		dbHost := os.Getenv("db_host")

		dbParams := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable host=%s", username, password, dbName, dbHost)
		conn, err := gorm.Open("postgres", dbParams)
	*/

	conn, err := gorm.Open("postgres", os.Getenv("DATABASE_URL"))

	if err != nil {
		panic(err)
	}
	db = conn
	db.AutoMigrate(&Review{})
}

func GetDB() *gorm.DB {
	return db
}
