package models

import (
	"github.com/jinzhu/gorm"
)

type Review struct {
	gorm.Model
	Title       string `gorm:"type:varchar(300);not null`
	Preview     string `gorm:"type:varchar(500);not null`
	Anime       string `gorm:"type:varchar(250);not null`
	Description string `gorm:"not null`
	Image       string
}
