package reviewcontrollers

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type JSONError struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

func writeEncoded(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
	fmt.Println(data)
}
