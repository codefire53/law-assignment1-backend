package reviewcontrollers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	reviewServices "gitlab.com/codefire53/law-assignment1-backend/services/review"
)

type ReviewController struct {
	ReviewService reviewServices.AbstractReviewService
}

type BaseReviewResponse struct {
	Error  *JSONError `json:"error,omitempty"`
	Status string     `json:"status,omitempty"`
}

func (rc ReviewController) GetReviews(w http.ResponseWriter, r *http.Request) {

	response, err := rc.ReviewService.GetReviews()

	if err != nil {
		writeReviewError(w, "invalid request", err)
		return
	}

	writeEncoded(w, response)
}

func (rc ReviewController) GetReviewDetail(w http.ResponseWriter, r *http.Request) {

	reviewID := parseID(r)
	response, err := rc.ReviewService.GetReviewDetail(reviewID)

	if err != nil {
		writeReviewError(w, "invalid request", err)
		return
	}

	writeEncoded(w, response)
}

func (rc ReviewController) CreateReview(w http.ResponseWriter, r *http.Request) {
	req, err := decodeRequest(r)
	if err != nil {
		writeReviewError(w, "invalid request", err)
		return
	}
	fmt.Println(req.Title)
	err = rc.ReviewService.CreateReview(req)
	if err != nil {
		writeReviewError(w, "review creation failed", err)
		return
	}
	writeEncoded(w, &BaseReviewResponse{
		Status: "review has been created",
	})
}

func (rc ReviewController) UpdateReview(w http.ResponseWriter, r *http.Request) {
	reviewID := parseID(r)
	req, err := decodeRequest(r)
	if err != nil {
		writeReviewError(w, "invalid request", err)
		return
	}
	err = rc.ReviewService.UpdateReview(reviewID, req)
	if err != nil {
		writeReviewError(w, "review update failed", err)
		return
	}
	writeEncoded(w, &BaseReviewResponse{
		Status: "review has been updated",
	})
}

func (rc ReviewController) DeleteReview(w http.ResponseWriter, r *http.Request) {
	reviewID := parseID(r)
	err := rc.ReviewService.DeleteReview(reviewID)
	if err != nil {
		writeReviewError(w, "review delete failed", err)
		return
	}
	writeEncoded(w, &BaseReviewResponse{
		Status: "review has been deleted",
	})
}

func writeReviewError(w http.ResponseWriter, errorCode string, err error) {
	e := &JSONError{Code: errorCode, Message: err.Error()}
	w.WriteHeader(http.StatusBadRequest)
	w.Header().Add("Content-Type", "application/json")
	writeEncoded(w, &BaseReviewResponse{Error: e, Status: "failed"})
}

func decodeRequest(r *http.Request) (*reviewServices.ReviewRequest, error) {
	req := new(reviewServices.ReviewRequest)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(req); err != nil {
		return nil, err
	}
	fmt.Println(req)
	validate := validator.New()
	err := validate.Struct(req)

	if err != nil {
		return nil, err
	}

	return req, nil
}

func parseID(r *http.Request) uint {
	res, _ := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
	return uint(res)
}
