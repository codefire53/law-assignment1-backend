package repositories

import (
	"gitlab.com/codefire53/law-assignment1-backend/models"
)

type ReviewRepositoryImpl struct {
}

type AbstractReviewRepository interface {
	GetByID(id uint) (*models.Review, error)
	GetAll() ([]models.Review, error)
	Create(review *models.Review) (*models.Review, error)
	Update(review *models.Review) (*models.Review, error)
	Delete(id uint) error
}

func (repo ReviewRepositoryImpl) GetByID(id uint) (*models.Review, error) {
	review := &models.Review{}
	err := models.GetDB().Where("id = ?", id).First(review).Error
	return review, err
}

func (repo ReviewRepositoryImpl) GetAll() ([]models.Review, error) {
	var reviews []models.Review
	err := models.GetDB().Find(&reviews).Error
	return reviews, err
}

func (repo ReviewRepositoryImpl) Create(review *models.Review) (*models.Review, error) {
	err := models.GetDB().Create(review).Error
	if err != nil {
		return nil, err
	}
	return review, err
}

func (repo ReviewRepositoryImpl) Update(review *models.Review) (*models.Review, error) {
	err := models.GetDB().Model(&review).Updates(&review).Error
	return review, err
}

func (repo ReviewRepositoryImpl) Delete(id uint) error {
	err := models.GetDB().Delete(&models.Review{}, id).Error
	return err
}
