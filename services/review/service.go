package reviewservices

import (
	"errors"
	"fmt"

	"gitlab.com/codefire53/law-assignment1-backend/models"
	"gitlab.com/codefire53/law-assignment1-backend/repositories"
)

type AbstractReviewService interface {
	GetReviews() ([]ReviewResponse, error)
	GetReviewDetail(id uint) (*ReviewResponse, error)
	UpdateReview(id uint, reviewRequest *ReviewRequest) error
	DeleteReview(id uint) error
	CreateReview(reviewRequest *ReviewRequest) error
}

type ReviewServiceImpl struct {
	ReviewRepository repositories.AbstractReviewRepository
}

type ReviewRequest struct {
	Title       string `json:"title,omitempty"`
	Preview     string `json:"preview,omitempty"`
	Anime       string `json:"anime,omitempty"`
	Description string `json:"description,omitempty"`
	Image       string `json:"image, omitempty`
}
type ReviewResponse struct {
	ID          uint   `json:"id,omitempty"`
	Title       string `json:"title,omitempty"`
	Preview     string `json:"preview,omitempty"`
	Anime       string `json:"anime,omitempty"`
	Description string `json:"description, omitempty"`
	Image       string `json:"image, omitempty`
}

func (service ReviewServiceImpl) GetReviews() ([]ReviewResponse, error) {
	reviews, err := service.ReviewRepository.GetAll()
	if err != nil {
		return nil, errors.New("Failed to fetch reviews")
	}
	reviewResponses := make([]ReviewResponse, len(reviews))
	for index, review := range reviews {
		reviewResponses[index] = ReviewResponse{
			ID:          review.ID,
			Title:       review.Title,
			Preview:     review.Preview,
			Anime:       review.Anime,
			Description: review.Description,
			Image:       review.Image,
		}
	}
	return reviewResponses, nil
}

func (service ReviewServiceImpl) GetReviewDetail(id uint) (*ReviewResponse, error) {
	review, err := service.ReviewRepository.GetByID(id)
	if err != nil {
		return nil, errors.New("Failed to fetch review")
	}
	reviewResponse := &ReviewResponse{
		ID:          review.ID,
		Title:       review.Title,
		Preview:     review.Preview,
		Anime:       review.Anime,
		Description: review.Description,
		Image:       review.Image,
	}
	return reviewResponse, nil
}

func (service ReviewServiceImpl) UpdateReview(id uint, reviewRequest *ReviewRequest) error {
	review, err := service.ReviewRepository.GetByID(id)
	if err != nil {
		return errors.New("Failed to fetch review")
	}

	updatedReview := &models.Review{
		Model:       review.Model,
		Title:       reviewRequest.Title,
		Preview:     reviewRequest.Preview,
		Anime:       reviewRequest.Anime,
		Description: reviewRequest.Description,
		Image:       reviewRequest.Image,
	}
	_, err = service.ReviewRepository.Update(updatedReview)
	if err != nil {
		return errors.New("Failed to update review")
	}
	return nil
}

func (service ReviewServiceImpl) DeleteReview(id uint) error {
	err := service.ReviewRepository.Delete(id)
	if err != nil {
		return errors.New("Failed to delete selected review")
	}
	return nil
}

func (service ReviewServiceImpl) CreateReview(reviewRequest *ReviewRequest) error {
	fmt.Println(reviewRequest.Title)
	newReview := &models.Review{
		Title:       reviewRequest.Title,
		Preview:     reviewRequest.Preview,
		Anime:       reviewRequest.Anime,
		Description: reviewRequest.Description,
		Image:       reviewRequest.Image,
	}
	_, err := service.ReviewRepository.Create(newReview)
	if err != nil {
		return errors.New("Failed to create review")
	}
	return nil
}
